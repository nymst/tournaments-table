import './App.css';
import TournamentsList from './components/TournamentsList/TournamentsList';

function App() {
  return (
    <div className="App">
      <TournamentsList />
    </div>
  );
}

export default App;