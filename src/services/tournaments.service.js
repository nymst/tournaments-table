import axios from 'axios';
import { config } from '../config/app.config';

export const getTournaments = async () => {
    const url = `${config.BASE_URL}/tournaments`;
    const headers = {
        'Content-Type': 'application/json',
        Accept: 'application/json',
        Authorization: `Basic ${config.AUTH_HEADER}`
    };

    try {
        const result = await axios.get(url, { headers });

        return result.data;
    } catch (e) {
        console.log(e);
        //add Toast 
    }
};

export const getCountries = (tournaments) => {
    const ids = [];
    const countriesArr = tournaments.map(el => el.country);
    const uniqueCountries = countriesArr.filter(el => {
        const isDuplicate = ids.includes(el.id);

        if (!isDuplicate) {
            ids.push(el.id);

            return true;
        }

        return false;
    });

    return uniqueCountries;
};