import './TournamentsList.scss';
import { useState } from 'react';
import { useEffect } from 'react';
import * as tournamentsService from '../../services/tournaments.service';

const TournamentsList = () => {
  const [tournaments, setTournaments] = useState([]);
  const [countries, setCountries] = useState([]);
  const [dropdownCountries, setDropdownCountries] = useState([]);
  const [selectedCountry, setSelectedCountry] = useState({});
  const [searchQuery, setSearchQuery] = useState('');
  const [searchFilterredTournaments, setSearchFilterredTournaments] = useState([]);
  const noResultsFoundBool = searchQuery.length > 1 && searchFilterredTournaments.length < 1;
  const tournamentsToSearch = searchFilterredTournaments?.length !== 0 ? searchFilterredTournaments : tournaments;

  useEffect(() => {
    const initData = async () => {
      const tournamentsData = await tournamentsService.getTournaments();
      const countriesData = tournamentsService.getCountries(tournamentsData);

      setTournaments(tournamentsData);
      setCountries(countriesData);
      setDropdownCountries(countriesData);
    };

    initData();
  }, []);

  useEffect(() => {
    const filterTournamentsFn = () => {
      const searchCases = {
        searchWithoutCountry: searchQuery?.length !== 0 && !selectedCountry?.hasOwnProperty('name'),
        countryWithoutSearch: searchQuery?.length === 0 && selectedCountry.hasOwnProperty('name'),
        combinedSearch: searchQuery?.length !== 0 && selectedCountry.hasOwnProperty('name')
      };
      const arrayCases = {
        searchWithoutCountry: tournaments.filter(el => el.name.toLowerCase().includes(searchQuery) || el.country.name.toLowerCase().includes(searchQuery)),
        countryWithoutSearch: tournaments.filter(el => el.country.id === selectedCountry.id),
        combinedSearch: searchFilterredTournaments.filter(el => el.name.toLowerCase().includes(searchQuery) && el.country.id === selectedCountry.id)
      };

      Object.entries(searchCases).some(el => {
        const searchCaseKey = el[0];
        const searchCaseConditionValue = el[1];

        if (searchCaseConditionValue === true) {
          setSearchFilterredTournaments(arrayCases[searchCaseKey]);
          
          return true;
        } else {
          setSearchFilterredTournaments([]);

          return false;
        }
      });

    };

    filterTournamentsFn();
  }, [searchQuery, selectedCountry]);

  const listTournamentsFn = () => {
    return tournamentsToSearch?.map((el, index) => {

      return (
        <tr className='tournament-single-entry' key={index}>
          <td className='tournament-title'>{el.name}</td>
          <td className='d-flex country-title'><img className='tournament-country-flag m-1' src={el.country?.url_flag} alt='' /> {el.country.name}</td>
          <td />
          <td />
          <td />
        </tr>
      )
    });
  };

  return (
    <div className='d-flex justify-content-center'>
      <table id='tournaments-table' className=' mt-4'>
        <thead>
          <tr>
            <th className='tournaments-col'>Tournament</th>
            <th className='countries-col'>Country</th>
            <th className='test-th'>
              <div className="input-group input-group-sm">
                <input
                  type="text"
                  value={searchQuery}
                  onChange={e => setSearchQuery(e.target.value.toLowerCase())}
                  placeholder='Search...'
                  className="form-control search-input"
                  aria-label="Sizing example input"
                  aria-describedby="inputGroup-sizing-sm" />
              </div>
            </th>
            <th>
              <div className="dropdown ">
                <button className="btn btn-secondary dropdown-toggle btn-dimensions" type="button" id="dropdownMenuButton2" data-bs-toggle="dropdown" aria-expanded="false">
                  {selectedCountry.hasOwnProperty('name')
                    ? <div className='d-flex align-items-center'>
                      <label>{selectedCountry.name}</label>
                      <img className='dropdown-tournament-country-flag' src={selectedCountry.url_flag} alt='' />
                    </div>
                    : <label>Choose country</label>
                  }
                </button>

                <ul className="dropdown-menu dropdown-menu-dark countries-dropdown" aria-labelledby="dropdownMenuButton2">
                  <li>
                    <input
                      type="text"
                      onChange={e => setDropdownCountries(countries.filter(el => el.name.toLowerCase().includes(e.target.value.toLowerCase())))}
                      placeholder='Search...'
                      className="form-control mb-2 dropdown-input"
                      aria-label="Sizing example input"
                      aria-describedby="inputGroup-sizing-sm" />
                  </li>
                  {dropdownCountries.map((country, index) => {
                    return (
                      <li key={index}>
                        <div className='d-flex m-1' onClick={() => setSelectedCountry(country)}>
                          <label className="dropdown-item d-flex" >
                            {country.name}
                            <img className='dropdown-tournament-country-flag' src={country.url_flag} alt='' />
                          </label>
                        </div>
                      </li>
                    )
                  })}
                </ul>
              </div>
            </th>
            <th className='clear-results-th'>
              {searchFilterredTournaments?.length !== 0 || selectedCountry.hasOwnProperty('name')
                ?
                <button type="button" className="btn btn-primary btn-dimensions" onClick={() => {
                  setSearchQuery('');
                  setSearchFilterredTournaments([]);
                  setSelectedCountry({});
                }}>
                  <label>Clear results</label>
                </button>
                : ''
              }
            </th>
          </tr>
        </thead>
        <tbody>
          {noResultsFoundBool
            ? <tr>
              <td />
              <label className='no-results-label mt-2'>No results found!</label>
              <td />
              <td />
              <td />
            </tr>
            : listTournamentsFn()}
        </tbody>
      </table>
    </div >
  )
};

export default TournamentsList;